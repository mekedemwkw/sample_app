class ApplicationMailer < ActionMailer::Base
  default from: 'swengineer5.whizkids@gmail.com'
  layout 'mailer'
end
